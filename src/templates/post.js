import React from "react"
import { graphql } from "gatsby"

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      frontmatter {
        title
        date
      }
      html
    }
  }
`

const Blog = props => {
  return (
    <div>
      <h1>
      {"This is the template" + props.data.markdownRemark.frontmatter.title}
      </h1>
      <div dangerouslySetInnerHTML={{__html: props.data.markdownRemark.html}} />
    </div>
    
  )
}

export default Blog
