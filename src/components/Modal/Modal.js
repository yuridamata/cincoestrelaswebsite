import React, { useContext } from "react"
import styles from "./modal.module.scss"

const Modal = props => {
  const preventPropagation = e => {
    e.stopPropagation()
    e.nativeEvent.stopImmediatePropagation()
  }

  return (
    <div className={props.visible ? styles.visible : styles.hidden}>
      <div className={styles.overlay}></div>
      <div className={styles.wrapper} onClick={props.hideModal}>
        <div className={styles.content} onClick={preventPropagation}>
          <div className={styles.closeButton} onClick={props.hideModal}>            
          </div>
          {props.children}
        </div>
      </div>
    </div>
  )
}

export default Modal
