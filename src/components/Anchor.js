import React from 'react';

const Anchor = props => {

  return <div  ref={props.ref}>{props.children}  </div>

}

export default Anchor;