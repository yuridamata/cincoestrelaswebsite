import React, {useState} from 'react';
import styles from './main_logo.module.scss';
import imgPaes from './img/site_5estrelas_paes_small.png';
import imgLogo from './img/site_5estrelas_logo_main.png'



const Mainlogo = (props, ref) => {


  return (
    <section ref={ref} className={styles.mainLogo} >
      <img className={styles.imgPao} src={imgPaes} alt="Imagem decorativa de pães e trigo"/>
      <img  className={styles.logo} src={imgLogo} alt="Logo da Cinco Estrelas"/>
      <div className={styles.padrao}/>
    </section>
  )
}

export default React.forwardRef(Mainlogo);