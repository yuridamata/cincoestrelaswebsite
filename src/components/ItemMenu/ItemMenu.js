import React, { useState } from "react"
import styles from "./itemMenu.module.scss"
const ItemMenu = props => {
  const [mostrar, setMostrar] = useState(false)

  const { kit } = props
  return (
    <div className={styles.itemMenu}>
      <h4 className={styles.titulo}>{`${kit.display} - R$ ${kit.preco}`}</h4>
      <div>{`${kit.qtdPessoas} ${
        kit.unidadePessoas ? kit.unidadePessoas : "pessoas"
      }`}</div>
      <div
        className={styles.triggerAccodion}
        onClick={() => setMostrar(!mostrar)}
      >
        <span className={mostrar ? styles.arrowDown : styles.arrowRight}></span>
        Produtos
      </div>
      <div
        className={`${styles.listaProdutos} ${mostrar ? styles.mostrar : ""}`}
      >
        {kit.produtos.map(produto => {
          return (
            <>
              <p className={styles.tituloProduto}>{typeof produto === "string" ? produto : produto.titulo}</p>
              {
                <p className={styles.opcoes} >{ produto.opcoes}</p>
              }
            </>
          )
        })}
      </div>
    </div>
  )
}

export default ItemMenu
