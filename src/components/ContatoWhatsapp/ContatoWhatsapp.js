import React, { useState, useEffect } from "react"
import styles from "./contatoWhatsapp.module.scss"

import qrCodeAguasClaras from "./img/qrCodeAguasClaras.png"
import qrCodeGuara from "./img/qrCodeGuara.png"

import whatsappLogo from "./img/whatsapp_logo.png"
import { BrowserView, MobileView } from "react-device-detect"

const dadosContatoLojas = {
  
  guara: {
    telefone: "5561984184681",
    display: "Guará",
    qrCode: qrCodeGuara,
    value: "guara",
  },
}

const ContatoWhatsapp = props => {
  const [lojaSelecionada, setlojaSelecionada] = useState({
    ...dadosContatoLojas.guara,
  })
  useEffect(() => {
    console.log("Alterada")
    console.log(lojaSelecionada)
  }, [lojaSelecionada])
  return (
    <div className={styles.whatsLinksWrapper}>
      <div className={styles.formLojas}>
        <label>
          <input
            type="radio"
            name="loja"
            value="guara"
            checked={lojaSelecionada.value === "guara"}
            onClick={() => setlojaSelecionada(dadosContatoLojas.guara)}
          />
          Guará
        </label>
      </div>

      <div className={styles.linkWhatsapp}>
        <BrowserView>          
          <img
            className={styles.qrCode}
            src={lojaSelecionada.qrCode}
            alt=""
          />
        </BrowserView>
        <MobileView>
          <div className={styles.linkWhatsapp}>
            <a target="_blank" href={`https://wa.me/${lojaSelecionada.telefone}`} rel="noopener noreferrer">
              <img className={styles.whatsappLogo} src={whatsappLogo} alt="" />
            </a>
          </div>
        </MobileView>
      </div>
    </div>
  )
}

export default ContatoWhatsapp
