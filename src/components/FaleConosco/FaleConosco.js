import React from "react"
import Modal from "../Modal/Modal"
import styles from './faleconosco.module.scss'


import imgAuxiliar from './img/img_auxiliar.jpg';
const FaleConosco = props => {
  return (
    <Modal
      visible={props.active}
      hideModal={() => {
        props.setActive(false)
      }}
    >
      <div className={styles.contentWrapper}>
        <div className={styles.imgWrapper}>
          <img src={imgAuxiliar}></img>
        </div>
        <div className={styles.contatosWrapper}>
          <h1>Fale Conosco</h1>
          <br />
          <p>No caso de dúvidas, reclamações ou sugestões, entre em contato conosco através do nosso SAC: <strong>sac@cincoestrelascasadepaes.com.br </strong></p>
          <br />
          <p>Caso queira fazer parte do nosss time, entre contato através do seguinte e-mail: <strong>rh@cincoestrelascasadepaes.com.br </strong></p>
        </div>

      </div>
    </Modal>
  )
}

export default FaleConosco
