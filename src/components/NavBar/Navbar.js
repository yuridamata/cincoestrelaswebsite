import React from "react"
import styles from "./navbar.module.scss"
import { PageContext } from "../../pages/index"
import logo  from './img/logo_azul.png'



const Navbar = (props) => {
  return (
    <PageContext>
      {({ scroll }) => {                


        const classes = scroll > 55 ? `${styles.nav} ${styles.visible}` : `${styles.nav}`
        return (
          <nav className={classes}>
            <ul className={styles.navItems}>
              <li onClick={() => props.scrollFunc(props.refs.inicioRef)} ><p>Início</p></li>
              <li onClick={() => props.setShowEncomendas(true)} ><p>Encomendas</p></li>
              <li onClick={() => props.scrollFunc(props.refs.promocoesRef)} ><p>Promoções</p></li>
              <li onClick={() => props.scrollFunc(props.refs.introducaoRef)}><p>História</p></li>
            </ul>
            <img className={styles.logo} src={logo} alt=""/>
            <ul className={styles.navItems}>
              <li onClick={() => props.scrollFunc(props.refs.blogRef)} ><p>Blog</p></li>
              <li onClick={() => props.scrollFunc(props.refs.parceirosRef)} ><p>Parceiros</p></li> 
              <li  onClick={() => props.scrollFunc(props.refs.mapRef)} ><p>Lojas</p></li>
              <li  onClick={() => props.setShowFaleConosco(true)} ><p>Fale Conosco</p></li>
            </ul>
          </nav>
        )
      }}
    </PageContext>
  )
}

export default Navbar
