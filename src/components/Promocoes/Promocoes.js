import React from "react"

import "react-responsive-carousel/lib/styles/carousel.min.css"
import { Carousel } from "react-responsive-carousel"
import { BrowserView, MobileView, isMobile } from "react-device-detect"

import style from "./promocoes.module.scss"

import maosCafeWeb from "./img/maos-cafe-web.jpg"
import maosCafeMobile from "./img/maos-cafe-mobile.jpg"

import paesWeb from "./img/banner-paes-web.jpg"
import paesMobile from "./img/banner-paes-mobile.jpg"



const Promocoes = (props, ref) => {
  return (
    <section ref={ref} className={style.promocoes}>
      <Carousel
        interval={2500}
        showArrows={true}
        showStatus={false}
        autoPlay={true}
        showThumbs={false}
      >
        <div>
          <img src={isMobile ? maosCafeMobile : maosCafeWeb} />
        </div>
        <div>
          <img src={isMobile ? paesMobile : paesWeb}/>
        </div>
      </Carousel>
    </section>
  )
}

export default React.forwardRef(Promocoes)
