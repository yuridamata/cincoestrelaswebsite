import React, { useState } from "react"
import styles from "./introducao.module.scss"

import imgSovandoMassa from "./img/sovando_massa.jpg"
import imgPadeiro from "./img/homem_forno.jpg"
import imgPaoCortado from "./img/pao_cortado.jpg"
import imgComposition from "./img/composition-modal.jpg"

import Modal from "../Modal/Modal"

import logo from "./img/logo.png"

const Introducao = (props, ref) => {
  const [showModal, setShowModal] = useState(false)
  return (
    <>
      <section ref={ref} className={styles.introducao}>
        <article className={styles.conteudo}>
          <h1>Prazer em te Servir</h1>
          <p>
            No ano de 1994, o casal José Maria Natalício e Margareth Bomtempo e
            seus quatro filhos deixaram a cidade de Tiros (MG) e vieram para
            Distrito Federal com o objetivo de vencer na vida. Devoto de Nossa
            Senhora Aparecida, José Maria sempre teve a certeza de que por meio
            do trabalho e da união construiria um patrimônio para ser o sustento
            da sua família. Tudo começou com uma simples padaria no Guará I,
            onde moravam e todos trabalhavam unidos.
          </p>
          <p>
            Com atitudes cuidadosas, como a de dar uma balinha a toda criança
            que entrasse na padaria e atender a todos com respeito e um sorriso,
            a família conquistou toda a clientela da região, clientes estes que
            até hoje são fiéis e todos os dias passam por lá.
          </p>
          <p>
            Atualmente, a Cinco Estrelas é administrada pelos filhos, gera
            emprego para mais de 200 colaboradores. Unidos, os filhos estão à
            frente do negócio e o conduzem com enorme gratidão, respeito e
            orgulho pelo empenho empregado pelos seus pais. “Acreditar que tudo
            é possível, com muito trabalho e dedicação, nos impulsiona a
            crescer, proporcionando sempre o melhor para nossos clientes e
            amigos”, diz uma das sócias.
          </p>
          <p>E para você, o que significa ter “CINCO ESTRELAS”?</p>
          <div className={styles.buttonWrapper}>
            <button
              className={styles.button}
              onClick={() => setShowModal(true)}
            >
              Nosso Valores
            </button>
          </div>
        </article>

        <div className={styles.composition}>
          <img
            className={styles.sovandoMassa}
            src={imgSovandoMassa}
            alt="Duas mãos sovando massa de pão."
          />
          <img
            className={styles.homemForno}
            src={imgPadeiro}
            alt="Homem incluindo algo em um fogão à lenha"
          />
          <img
            className={styles.paoCortado}
            src={imgPaoCortado}
            alt="Um pão de forma fatiado"
          />
        </div>
      </section>

      <Modal visible={showModal} hideModal={() => setShowModal(false)}>
        <div className={styles.modal}>
          <div className={styles.modalContent}>
            <div className={styles.logoWrapper}>
              <img className={styles.imgLogo} src={logo} alt="" />
            </div>

            <h1>Nossos Valores</h1>
            <div className={styles.valores}>
              
              <div className={styles.valor}>
                <h2 className={styles.tituloValor}>Missão</h2>
                <p className={styles.tituloConteudo}>
                  Tornar a vida das pessoas mais próximas e ser uma ponte par realização de sonhos.
                </p>
              </div>

              <div className={styles.valor}>
                <h2 className={styles.tituloValor}>Visão</h2>
                <p className={styles.tituloConteudo}>
                  Ser referência em Casa de Pães no Brasil para consumir, frequentar e trabalhar.
                </p>
              </div>

              <div className={styles.valor}>
                <h2 className={styles.tituloValor}>Valores</h2>
                <p className={styles.tituloConteudo}>
                  Família, Compromentimento, Excelência, Dedicação, Caráter e Paixão em tudo que se faz.
                </p>
              </div>        
            </div>
          </div>

          <div className={styles.compositionModal}>
            <img
              className={styles.imgComposition}
              src={imgComposition}
              alt=""
            />
          </div>
        </div>
      </Modal>
    </>
  )
}

export default React.forwardRef(Introducao)
