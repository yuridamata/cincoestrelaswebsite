import React from "react"
import styles from "./loja.module.scss"
import imgWaze from "./img/waze.png"
import imgMaps from "./img/google-maps.png"

const Loja = props => {
  return (
    <div className={styles.loja}>
      <h1>{props.nome}</h1>
      <address>{props.endereco}</address>
      <p>{props.telefone}</p>
      <p>{props.horarioFunc}</p>
      <div className={styles.directions}>
        {/* <img src={imgWaze} alt="" />
        <img src={imgMaps} alt="" /> */}
      </div>
    </div>
  )
}

export default Loja
