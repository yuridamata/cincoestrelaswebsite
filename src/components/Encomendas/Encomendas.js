import React, { useState, useEffect } from "react"
import styles from "./encomendas.module.scss"
import Modal from "../Modal/Modal"
import ItemMenu from "../ItemMenu/ItemMenu"
import QRCode from "qrcode.react"
import ContatoWhatsapp from "../ContatoWhatsapp/ContatoWhatsapp"
import EncomendasForm from "./EncomendasForm"
import { BrowserView, MobileView } from "react-device-detect"
import whatsapp from "../Footer/img/whatsapp.png"
import logo from "./img/logo-mini.jpg"
const ecommerceURL = "http://cincoestrelascasadepaes.com.br/ecommerce"
const commonOpcoes = {
  microTorta:
    "Bis, Brigadeiro, Confetti, Kit Kat, Maracujá, Suflair ou Tapete de Morango",
  salgadinhosFestinha: "25 fritos e 25 assados",
  docinhoFestinha: "Docinho Brigadeiro",
  bebidaFestinha: "Refrigerante 2l ou Suco 1l",

  tortaAltinha: "Suflair, Brigadeiro/ Brigadeiro c/ beijinho/ Tapete Morango",

  bebida02:
    "Pode escolher duas bebidas, sendo uma opção de refrigerante e uma opção de suco (Coca Cola 2 Lt OU Guaraná Antárctica 2 Lt OU Suco laranja 1 Lt ou Suco Goiaba 1 Lt)",

  salgadinhoFrito:
    "Fritos: Coxinha, Kibe, Enroladinho salsicha, Bolinha de queijo",

  salgadinhoAssado:
    "Assados: Enroladinho salsicha, Esfirra carne, empada massa podre, mini pão pizza, Pãozinho da vovó.",

  tortaTradicional: "Brigadeiro, Brigadeiro c/ beijinho, Mousse limão, Frutas.",

  docinhosTradicionais: "Escolha entre dois sabores",

  bebidas03: "Refrigerante de 2 Lt, ou Suco de 1 Lt",
}

const kitsFesta = [
  {
    display: "Kit Festa 1",
    qtdPessoas: "2",
    preco: "49,00",
    produtos: [
      {
        titulo: "01 Micro Torta Doce - 250 a 300 gramas",
        opcoes: commonOpcoes.microTorta,
      },
      {
        titulo: "25 Salgadinhos fritos e assados",
      },

      { titulo: "08 Brigadeiros" },
      {
        titulo: "01 Bebida",
        opcoes: commonOpcoes.bebidaFestinha,
      },
    ],
  },

  {
    display: "Kit Festa 2",
    qtdPessoas: "4",
    preco: "69,90",
    produtos: [
      {
        titulo: "01 Micro Torta Doce - 250 a 300 gramas",
        opcoes: commonOpcoes.microTorta,
      },
      { titulo: "50 Salgadinhos", opcoes: commonOpcoes.salgadinhosFestinha },
      { titulo: "16 Brigadeiros" },
      { titulo: "02 Bebidas", opcoes: commonOpcoes.bebidaFestinha },
    ],
  },

  {
    display: "Kit Festa 3",
    qtdPessoas: "5 a 8",
    preco: "119,90",
    produtos: [
      {
        titulo: "01 Torta Altinha - 800 gramas",
      },
      { titulo: "75 Salgadinhos", opcoes: "50 fritos e 25 assados" },
      { titulo: "20 Brigadeiros" },
      { titulo: "02 Bebidas", opcoes: commonOpcoes.bebida02 },
    ],
  },

  {
    display: "Kit Festa 4",
    qtdPessoas: "10",
    preco: "169,90",
    produtos: [
      {
        titulo: "1kg de Torta Doce Tradicional",
      },
      { titulo: "100 Salgadinhos", opcoes: "50 fritos e 50 assados" },

      {
        titulo: "40 docinhos tradicionais",
        opcoes: commonOpcoes.docinhosTradicionais,
      },
      {
        titulo: "2 Bebidas ",
        opcoes: commonOpcoes.bebidas02,
      },
    ],
  },

  {
    display: "Kit Festa 5",
    qtdPessoas: "15",
    preco: "249,90",
    produtos: [
      {
        titulo: "1,5kg de Torta Doce Tradicional",
      },
      {
        titulo: "100 Salgadinhos Fritos",
        opcoes: "Escolha entre dois sabores",
      },
      {
        titulo: "60 Docinhos Tradicionais",
        opcoes: "Escolha entre três sabores",
      },

      {
        titulo: "3 Bebidas (Refrigerante de 2L ou Suco de 1L)",
        opcoes: commonOpcoes.bebidas02,
      },
    ],
  },
  {
    display: "Kit Festa 6",
    qtdPessoas: "20",
    preco: "339,90",
    produtos: [
      {
        titulo: "2kg de Torta Doce Tradicional",
      },
      {
        titulo: "100 Salgadinhos Fritos",
      },
      {
        titulo: "100 Salgadinhos Assados",
        opcoes: "Escolha entre quatro sabores",
      },
      {
        titulo: "80 Docinhos Tradicionais",
        opcoes: "Escolha entre quatro sabores",
      },

      {
        titulo: "4 Bebidas",
        opcoes: commonOpcoes.bebidas02,
      },
    ],
  },
  {
    display: "Kit Festa 7",
    qtdPessoas: "25",
    preco: "419,90",
    produtos: [
      {
        titulo: "2,5kg de Torta Doce Tradicional",
      },
      {
        titulo: "150 Salgadinhos Fritos",
        opcoes: "Escolha entre 6 sabores.",
      },
      {
        titulo: "100 Salgadinhos Assados",
        opcoes: "Escolha entre 6 sabores.",
      },
      {
        titulo: "100 Docinhos Tradicionais",
        opcoes: "Escolha entre 5 sabores.",
      },

      {
        titulo: "5 Bebidas ",
        opcoes: commonOpcoes.bebidas02,
      },
    ],
  },
  {
    display: "Kit Festa 8",
    qtdPessoas: "30",
    preco: "489,90",
    produtos: [
      {
        titulo: "3kg de Torta Doce Tradicional",
      },
      {
        titulo: "200 Salgadinhos Fritos",
        opcoes: "Escolha entre dois sabores",
      },
      {
        titulo: "100 Salgadinhos Assados",
        opcoes: "Escolha entre dois sabores",
      },
      {
        titulo: "120 Docinhos Tradicionais",
        opcoes: "Escolha entre 6 sabores",
      },

      {
        titulo: "6 Bebidas",
        opcoes: commonOpcoes.bebidas02,
      },
    ],
  },
  {
    display: "Kit Festa 9",
    qtdPessoas: "40 a 50",
    preco: "789,90",
    produtos: [
      {
        titulo: "4kg de Torta Doce Tradicional",
      },
      {
        titulo: "200 Salgadinhos Assados",
        opcoes: "Escolha entre oito sabores",
      },
      {
        titulo: "200 Salgadinhos Fritos",
        opcoes: "Escolha entre seis sabores",
      },
      {
        titulo: "200 Docinhos Tradicionais",
        opcoes: "Escolha entre dez sabores",
      },
      {
        titulo: "9 Bebidas",
        opcoes: commonOpcoes.bebidas02,
      },
      "1 Pão de metro tradicional",
    ],
  },
  {
    display: "Kit Festa - Infantil",
    qtdPessoas: "20 a 25",
    unidadePessoas: "Pessoas",
    preco: "349,90",
    produtos: [
      {
        titulo: "2kg de Torta Doce Tradicional",
      },
      {
        titulo: "100 Salgadinhos Assados",
        opcoes: "Escolha entre quatro sabores",
      },
      {
        titulo: "100 Salgadinhos Fritos",
        opcoes: "Escolha entre quatro sabores",
      },

      {
        titulo: "100 Docinhos Tradicionais",
        opcoes: "Escolha entre cinco sabores",
      },
      {
        titulo: "3 Bebidas",
        opcoes: commonOpcoes.bebidas02,
      },
      "1 Vela (Brinde)",
    ],
  },

  {
    display: "Kit Café da Manhã",
    qtdPessoas: "20 a 25",
    preco: "349,90",
    produtos: [
      {
        titulo: "50 Salgados Assados",
        opcoes: "Escolha entre dois sabores",
      },
      "01 Bolo caseiro redondo",
      "50 Pães de Queijo Tradicional",
      "50 Rosquinhas Hungaras",
      "50 Mini sanduíches americanos",
      "01 Pão de metro tradicional",
      "02 Achocolatados de 1L",
      "02 Chás Gelados Tradicional de 1L",
      "05 Sucos Cinco Estrelas de 1L",
    ],
  },
]

const Encomendas = props => {
  const [aba, setAba] = useState("whatsapp")
  const arrayKits = kitsFesta.map(kit => <ItemMenu kit={kit} />)

  useEffect(() => {
    if (props.active) {
      document.body.style.overflow = "hidden"
    } else {
      document.body.style = ""
    }
  }, [props.active])

  const mudaAba = novaAba => {
    setAba(novaAba)
  }

  return (
    <>
      <div
        className={`${styles.encomendasButton}`}
        role="button"
        onClick={() => props.setActive(!props.active)}
        onKeyDown={() => props.setActive(!props.active)}
      >
        <p>Encomendas</p>
      </div>
      <div className={`${styles.whatsappBtn}`}>
        <a href="https://wa.me/5561984184681" target="_blank">
          <img src={whatsapp} alt="" />
        </a>
      </div>

      <Modal
        visible={props.active}
        hideModal={() => {
          props.setActive(false)
        }}
      >
        <div className={styles.modalContent}>
          <section className={styles.instrucoes}>
            <h1> Orientações Gerais</h1>
            <p>&nbsp;</p>
            <p>
              Envie seu pedido para nós e entraremos em contato para que efetue
              sua encomenda.{" "}
              <strong>
                Mas lembre-se, caso seu pedido seja urgente, ligue diretamente
                para nossa loja e faça sua encomenda
              </strong>
              . É importante que a encomenda seja feita com pelo menos 24h de
              antecedência por telefone e 48 horas por e-mail, para que possamos
              atender suas expectativas.
            </p>
            <p>&nbsp;</p>
            <p>
              Para que sua encomenda seja efetivada você deverá efetuar o
              pagamento de 50% do valor da encomenda presencialmente, por
              transferência bancária ou pelo link de pagamentos da Cielo.
            </p>

            <p>&nbsp;</p>
            <p>
              Formas de Pagamento: Dinheiro, cartão de crédito e débito,
              alimentação e refeição e transferência bancária.
            </p>
            <p>&nbsp;</p>
            <p>Encomendas via telefone das 7h às 20h.</p>
            <p>&nbsp;</p>
          </section>

          <section className={styles.cardapio}>
            <h1> Cardápio</h1>

            {arrayKits}
          </section>

          <section className={styles.tiposContato}>
            <nav>
              <ul className={styles.abas}>
                {/* <li
                  className={aba === "ecommerce" ? styles.active : ""}
                  onClick={() => mudaAba("ecommerce")}
                >
                  Ecommerce
                </li> */}
                <li
                  className={aba === "whatsapp" ? styles.active : ""}
                  onClick={() => mudaAba("whatsapp")}
                  onKeyDown={() => mudaAba("whatsapp")}
                >
                  Whatsapp
                </li>
              </ul>
            </nav>

            <article
              className={` ${styles.abaContent} ${styles.whatsapp} ${
                aba === "whatsapp" ? styles.abaActive : styles.abaInactive
              }`}
            >
              <article className={styles.instrucoes}>
                <p>
                  Horário de atendimento: Segunda a Domingo das 7h ás 20h, com
                  retorno em até 24h.
                </p>
                <p>&nbsp;</p>
                <p>Lojas que atendem via whatsapp:</p>
                <p>&nbsp;</p>
                <p>GUARA I: 61-33814965/ 98418-4681 (Whatsapp)</p>
                <p>&nbsp;</p>
                Escaneie o QR Code ao lado para falar nos contatar via whatsapp
                através do celular.
                <p>&nbsp;</p>
                <p>
                  Caso prefira, pode nos contatar via Whatsapp Web clicando no
                  ícone abaixo ou através do aplicativo escaneando o QR Code
                  utilizando a câmera de seu celular.
                </p>
              </article>
              <ContatoWhatsapp />
            </article>

            <article
              className={` ${styles.abaContent} ${styles.formulario} ${
                aba === "formulario" ? styles.abaActive : styles.abaInactive
              }`}
            >
              <p className={styles.instrucoes}>
                Encomendas via e-mail ou formulário, só devem ser consideradas
                válidas após contato telefônico de nossa equipe.
              </p>
              <p>&nbsp;</p>
              <p className={styles.instrucoes}>
                E-mail: encomendas-site@cincoestrelascasadepaes.com.br
              </p>

              <div className={styles.errosForm}>* Preencha todos os campos</div>

              <p>&nbsp;</p>

              <EncomendasForm />
            </article>
            <article
              className={` ${styles.abaContent} ${styles.formulario} ${
                aba === "ecommerce" ? styles.abaActive : styles.abaInactive
              }`}
            >
              <BrowserView>
                <div className={styles.instrucoesEcommerce}>
                  <p className={styles.instrucoes}>
                    Conheça agora o nosso e-commerce, disponível nas versões Web
                    e mobile!
                  </p>

                  <p className={styles.instrucoes}>
                    Para acessá-lo via web, clique na logo cinco estrelas abaixo
                    <a
                      style={{ display: "contents" }}
                      target="_blank"
                      href="https://cincoestrelascasadepaes.com.br/ecommerce"
                    >
                      <img className={styles.logo} src={logo} alt="" />
                    </a>
                  </p>
                  <p className={styles.instrucoes}>
                    Para acessá-lo via celular, escaneie o QR Code abaixo com a
                    câmera do seu aparelho.
                  </p>
                  <p>&nbsp;</p>
                </div>
                <div className={styles.qrCodeWrapper}>
                  <QRCode
                    value={ecommerceURL}
                    style={{ alignSelf: "center" }}
                    size={192}
                  />
                </div>
              </BrowserView>
              <MobileView>
                <div className={styles.instrucoesEcommerce}>
                  <p className={styles.instrucoes}>
                    Conheça agora o nosso e-commerce. Para acessá-lo, clique na
                    imagem abaixo.
                  </p>
                  <p>&nbsp;</p>
                </div>
                <div className={styles.linkWhatsapp}>
                  <a
                    className={styles.instrucoes}
                    target="_blank"
                    href={ecommerceURL}
                    rel="noopener noreferrer"
                  >
                    <img className={styles.logo} src={logo} />
                  </a>
                </div>
              </MobileView>
            </article>
          </section>
        </div>
      </Modal>
    </>
  )
}

export default Encomendas
