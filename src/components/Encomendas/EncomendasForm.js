import React, { useState } from "react"
import Encomendas from "./Encomendas"
import axios from "axios"
import styles from "./encomendasForm.module.scss"

import ReactLoading from "react-loading"

// const fetchUrl = "http://localhost:3333/mail/send"
const fetchUrl = "http://labsb.tech/api/mail/send";

const formFields = [
  { label: "Nome", name: "nome", className: styles.formItem },
  { label: "Telefone", name: "telefone", className: styles.formItem },
  { label: "E-mail", name: "email", className: styles.formItem },
  {
    label: "Pedido",
    name: "pedido",
    className: styles.formItem,
    type: "textArea",
  },
]

const EncomendasForm = props => {
  const [enviando, setEnviando] = useState(false)
  const [dadosForm, setDadosForm] = useState({
    nome: "",
    telefone: "",
    email: "",
    pedido: "",
  })

  const sendMail = async () => {
    for (let campo in dadosForm) {
      if (dadosForm[campo] === "") {
      }
    }

    setEnviando(true)
    try {
      const response = await axios.post(fetchUrl, {
        ...dadosForm,
      })
    } catch (err) {}

    setEnviando(false)
  }

  const renderFields = () => {
    return formFields.map(field => {
      const updateFunc = (campo, valor) => {
        setDadosForm(prevState => {
          return { ...prevState, [campo]: valor }
        })
      }

      switch (field.type) {
        case "textArea":
          return (
            <div className={styles.formItem}>
              <label className={styles.label} for={field.name}>
                {field.label}
              </label>
              <br />
              <textarea
                onBlur={e => {
                  updateFunc(field.name, e.target.value)
                }}
                rows="15"
                className={styles.textarea}
                type="text"
                name="nome"
              />
            </div>
          )
        default:
          return (
            <div className={styles.formItem}>
              <label className={styles.label} for={field.name}>
                {field.label}
              </label>
              <br />
              <input
                onBlur={e => {
                  updateFunc(field.name, e.target.value)
                }}
                className={styles.input}
                type="text"
                name={field.name}
              />
            </div>
          )
      }
    })
  }

  //Caso esteja enviando
  if (enviando) {
    return (
      <div className={styles.loadingWrapper}>
        <ReactLoading
          className={styles.loading}
          type={"spinningBubbles"}
          color={"#002856"}
          height={"5%"}
          width={"10%"}
        />
      </div>
    )
  }
  return (
    <>
      <form>{renderFields()}</form>
      <button onClick={() => sendMail()} className={styles.buttonFormulario}>
        Enviar
      </button>
    </>
  )
}

export default EncomendasForm
