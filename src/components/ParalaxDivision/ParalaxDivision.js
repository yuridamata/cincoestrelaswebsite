import React from "react"
import styles from "./paralax_division.module.scss"

const ParalaxDivision = props => {
  return (
    <div
      className={styles.paralax}
      style={{ backgroundImage: `url(${props.img})` }}
    >
      <div className={styles.overlay}></div>
    </div>
  )
}

export default ParalaxDivision
