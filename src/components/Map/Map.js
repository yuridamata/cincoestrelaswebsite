import React, { useState } from "react"
import styles from "./map.module.scss"

const Map = (props, ref) => {
  const [clicked, setClicked] = useState(false)

  return (
    <div ref={ref} className={styles.mapContainer}  onMouseLeave={ () => setClicked(false)  } >
      <div
        className={clicked ? styles.clicked : styles.unclicked}
        onClick={() => setClicked(true)}
       
      > <p>Clique para ativar a navegação no mapa</p></div>
      <iframe
        src="https://www.google.com/maps/d/embed?mid=1wBItVxLjkjnIS2ReKaX8Q9EZAS_unR2F"
        width="100%"
        height="100%"
      ></iframe>
    </div>
  )
}

export default React.forwardRef(Map)
