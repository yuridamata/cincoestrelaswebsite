import React, { useState, useEffect, useRef } from "react"
import { graphql, useStaticQuery } from "gatsby"
import styles from "./posts.module.scss"
import Modal from "../Modal/Modal"

import videoPadeiro from "./img/video_padeiro.png"
import videoLancamento from "./img/video_lancamento.png"
import capaInstitucional from "./img/capa_institucional.png"

import mp4Marca from "../../pages/videos/video_padeiro.mp4"
import mp4Institucional from "../../pages/videos/video_mudanca_marca.mp4"
import mp4Padeiro from "../../pages/videos/video_padeiro.mp4"

const Post = props => {
  const [showModal, setShowModal] = useState(false)
  const videoRef = useRef(null)
  const { postData, img } = props

  return (
    <article className={styles.post}>
      <img
        className={styles.imgPost}
        src={img}
        alt=""
        style={postData.border ? { border: "2px solid #ca8342" } : {}}
      />
      <h1>{postData.title}</h1>
      <p>{postData.call}</p>
      <button
        onClick={() => {
          setShowModal(true)
        }}
      >
        Saiba Mais
      </button>
      <Modal
        key={postData.title}
        visible={showModal}
        hideModal={() => {
          if (videoRef && videoRef.current && videoRef.current.pause) {
            videoRef.current.pause()
          }
          setShowModal(false)
        }}
      >
        {postData.md ? (
          <div className={styles.modal}>
            <img src={img} alt="" />
            <h1>{postData.title}</h1>
            <div dangerouslySetInnerHTML={{ __html: postData.html }} />
          </div>
        ) : (
          <>
            <h1>{postData.title}</h1>
            <video
              ref={videoRef}
              key={postData.title}
              className={styles.video}
              width="320"
              height="240"
              src={postData.video}
              controls
              autoplay
            >
              Your browser does not support the video tag.
            </video>
          </>
        )}
      </Modal>
    </article>
  )
}

const PostList = (props, ref) => {
  const [postAtual, setPostAtual] = useState(null)
  const videoRefPadeiro = useRef(null)
  const videoRefLancamento = useRef(null)

  useEffect(() => {
    if (postAtual === null) {
      document.body.style = ""
    } else {
      console.log("postAtual.title")
      console.log(postAtual.title)
      document.body.style.overflow = "hidden"
    }
  }, [postAtual])

  const data = useStaticQuery(graphql`
    query {
      allMarkdownRemark {
        edges {
          node {
            frontmatter {
              title
              date
              img
              call
            }
            html
            fields {
              slug
            }
          }
        }
      }
    }
  `)

  const postsData = data.allMarkdownRemark.edges.map(data => {
    return {
      title: data.node.frontmatter.title,
      slug: data.node.fields.slug,
      img: data.node.frontmatter.img,
      call: data.node.frontmatter.call,
      html: data.node.html,
      md: true,
    }
  })

  //Inclusão de Posts que não estão no MarkDown
  postsData.push({
    title: "Vídeo institucional.",
    call: "Vídeo institucional da Cinco Estrelas.",
    date: "2021-03-23",
    border: true,
    img: capaInstitucional,
    video: mp4Institucional,
    md: false,
  })

  postsData.push({
    title: "Palavras do novo chefe.",
    call:
      "Apresentação do novo chefe da padaria Cinco Estrelas, Bernardo, responsável pelos pães artesanais da casa.",
    date: "2019-04-04",
    img: videoPadeiro,
    video: mp4Padeiro,
    md: false,
  })
  postsData.push({
    title: "Lançamento nova marca.",
    call: "Vídeo de lançamento da nova marca Cinco Estrelas.",
    date: "2019-04-04",
    img: videoLancamento,
    video: mp4Marca,
    youtubeUrl: "https://www.youtube.com/embed/0cQAYubZ-DY",
    ref: videoRefLancamento,
    md: false,
  })

  const arrayPosts = []

  for (let postData of postsData) {
    const img = postData.md
      ? require(`../../posts/img/${postData.img}`)
      : postData.img

    arrayPosts.push(<Post img={img} postData={postData} />)
  }
  return (
    <section ref={ref} className={styles.postList}>
      {arrayPosts}
    </section>
  )
}

export default React.forwardRef(PostList)
