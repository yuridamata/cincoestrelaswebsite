import React from "react"
import styles from "./parceiros.module.scss"

import americanSchool from "./img/american_school.png"
import amil from "./img/amil.png"
import annaNery from "./img/anna_nery.png"
import colegioBiangulo from "./img/colegio_biangulo.png"
import colegioMarista from "./img/colegio_marista.png"
import dfStar from "./img/df_star.png"
import donaChicaCafe from "./img/dona_chica_cafe.png"
import hcPneus from "./img/hc_pneus.png"
import hospitalAlvorada from "./img/hospital_alvorada.png"
import hospitalBrasilia from "./img/hospital_brasilia.png"
import hostpitalAnchieta from "./img/hostpital_anchieta.png"
import jcDecaux from "./img/jc_decaux.png"
import maternidadeBrasilia from "./img/maternidade_brasilia.png"
import pedacinhoDoCeu from "./img/pedacinho_do_ceu.png"
import sabin from "./img/sabin.png"
import santaLucia from "./img/santa_lucia.png"
import santaHelena from './img/santa_helena.jpg'
import festasCritativas from './img/festas_criativas.png'

const Parceiros = (props, ref) => {
  return (
    <section ref={ref} className={styles.parceiros}>
      <h1 className={styles.titulo}> Nosso Parceiros</h1>

      <div className={styles.logos}>
        <img src={hostpitalAnchieta} alt="" />
        <img src={hospitalAlvorada} alt="" />
        <img src={hospitalBrasilia} alt="" />
        <img src={maternidadeBrasilia} alt="" />

        <img src={sabin} alt="" />
        <img src={annaNery} alt="" />
        <img src={santaLucia} alt="" />
        <img src={colegioMarista} alt="" />

        <img src={americanSchool} alt="" />
        <img src={amil} alt="" />
        <img src={hcPneus} alt="" />
        <img src={jcDecaux} alt="" />

        <img src={dfStar} alt="" />
        <img src={colegioBiangulo} alt="" />
        <img src={pedacinhoDoCeu} alt="" />
        <img src={donaChicaCafe} alt="" />

        <img src={santaHelena} alt="" />
        <img src={festasCritativas} alt="" />

      </div>
      <h3 className={styles.subtitulo}>
        {" "}
        Muito além de produtos, servimos "esperança"
      </h3>
    </section>
  )
}

export default React.forwardRef(Parceiros)
