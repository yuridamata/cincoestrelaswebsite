import React from "react"

import styles from "./footer.module.scss"

import imgFacebook from "./img/facebook.png"
import imgInstagram from "./img/instagram.png"
import imgTripAdv from "./img/trip-advisor.png"


import Loja from "../Loja/Loja"

const dadosLojas = [
  {
    nome: "Guará I",
    endereco: "QI 04 - Bloco B - Lojas 1/3",
    horarioFunc: "6h as 22h",
    telefone: "(61) 3381-4965",
    mapsUrl: "mapsURL",
    wazeUrl: "wazeURL",
  },
  {
    nome: "Guará II",
    endereco: "AE 04 - lote G/H - Residencial Olympique",
    telefone: "(61) 3381-4961",
    horarioFunc: "6h as 22h",
    mapsUrl: "mapsURL",
    wazeUrl: "wazeURL",
  },
  
  {
    nome: "Águas Claras",
    endereco: "Q. 301, Rua A, conjunto 02, lote 01 lojas 02 e 04 - Avenida Parque Águas Claras",
    telefone: "(61) 3201-3672",
    horarioFunc: "6h as 22h",
    mapsUrl: "mapsURL",
    wazeUrl: "wazeURL",
  },
  {
    nome: "Águas Claras II",
    endereco: "Avenida Pau Brasil lote 10 lojas 38, 39 e 40 - Edificio Le Quartier",
    horarioFunc: "6h as 22h",
    telefone: "(61) 3247-0200",
    mapsUrl: "mapsURL",
    wazeUrl: "wazeURL",
  },
  {
    nome: "Sudoeste",
    endereco: "CLSW 300 B Bloco 1 edifício Morro de São Paulo - loja 29",
    telefone: "(61) 98626- 0137 (Somente Whatsapp)",
    horarioFunc: "6h as 22h",
    mapsUrl: "mapsURL",
    wazeUrl: "wazeURL",
  },
  {
    nome: "Noroeste",
    endereco: "CLNW 08/09, Loja 07, Projeção C, Setor de Habitações Coletivas Noroeste",
    telefone: "(61) 98445-0546 (Somente Whatsapp)",
    horarioFunc: "7h as 21h",
    mapsUrl: "mapsURL",
    wazeUrl: "wazeURL",
  },
]

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <h1  className={styles.tituloFooter}  >  Nossas Lojas</h1>
      <section className={styles.lojas}>
        {dadosLojas.map(loja => {
          return <Loja {...loja} />
        })}
      </section>
      <section className={styles.socialMedia}>
        <a href="https://www.instagram.com/cincoestrelascasadepaes/"  target="_blank" ><img src={imgInstagram} alt="" /></a>
        <a href="https://www.facebook.com/cincoestrelascasadepaes/" target="_blank" ><img src={imgFacebook} alt="" /></a>
        <a href="https://www.tripadvisor.com.br/Restaurant_Review-g303322-d8157492-Reviews-Cinco_Estrelas_Casa_de_Paes-Brasilia_Federal_District.html" target="_blank" ><img src={imgTripAdv} alt="" /></a>
      </section>
    </footer>
  )
}

export default Footer
