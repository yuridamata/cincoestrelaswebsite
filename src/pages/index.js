import React, { useEffect, createContext, useState, useRef } from "react"
// import { Document, Page } from "react-pdf"
import Layout from "../components/Layout/Layout"
import indexStyles from "./index.module.scss"

import { Helmet } from "react-helmet"
import Mainlogo from "../components/MainLogo/MainLogo.js"
import Promocoes from "../components/Promocoes/Promocoes"
import Introducao from "../components/Introducao/Introducao"
import ParalaxDivision from "../components/ParalaxDivision/ParalaxDivision"
import Map from "../components/Map/Map"
import Parceiros from "../components/Parceiros/Parceiros"
import Footer from "../components/Footer/Footer"
import Navbar from "../components/NavBar/Navbar"
import favicon from "./img/favicon.ico"
import Modal from "../components/Modal/Modal"
import Encomendas from "../components/Encomendas/Encomendas"
import FaleConosco from "../components/FaleConosco/FaleConosco"

import { isMobile } from "react-device-detect"

//Imagens
import paralax1_small from "./img/paralax1_small.png"
import paralax1 from "./img/paralax1.png"
import paralax2 from "./img/paralax2.png"
import PostList from "../components/PostList/PostList"

import video from "./videos/video_mudanca_marca.mp4"

const PageContext = createContext({
  scroll: 0,
  refs: {},
})

const IndexPage = () => {
  const [scroll, setScroll] = useState(0)
  const [showModal, setShowModal] = useState(true)
  const [muted, setMuted] = useState(false)
  const [showEncomendas, setShowEncomendas] = useState(false)
  const [showFaleConosco, setShowFaleConosco] = useState(false)
  const [numPages, setNumPages] = useState(null)
  const [pageNumber, setPageNumber] = useState(1)

  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages)
  }

  useEffect(() => {
    //Necessário par afazer o build do gatsby
    window.addEventListener("scroll", () => setScroll(window.scrollY))
    document.body.style = ""
    document.body.style.overflow = "hidden"
    setShowModal(true)
  }, [])

  useEffect(() => {
    if (showModal) {
      document.body.style.overflow = "hidden"
    } else {
      document.body.style = ""
    }
  }, [showModal])

  const inicioRef = useRef(null)
  const promocoesRef = useRef(null)
  const introducaoRef = useRef(null)
  const blogRef = useRef(null)
  const parceirosRef = useRef(null)
  const mapRef = useRef(null)
  const videoRef = useRef(null)

  const scrollToRef = ref => {
    window.scrollTo({ top: ref.current.offsetTop - 100, behavior: "smooth" })
  }

  // const youtubeUrl = "https://www.youtube-nocookie.com/embed/10dNmAz2B94?rel=0"
  const youtubeUrl = "https://player.vimeo.com/video/418914918"

  return (
    <PageContext.Provider value={{ scroll }}>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Cinco Estrelas Sua Casa desde 1994</title>
        <link rel="icon" href={favicon} />
      </Helmet>
      <Navbar
        showEncomendas={showEncomendas}
        setShowEncomendas={setShowEncomendas}
        showFaleConosco={showFaleConosco}
        setShowFaleConosco={setShowFaleConosco}
        refs={{
          inicioRef,
          promocoesRef,
          introducaoRef,
          blogRef,
          mapRef,
          parceirosRef,
        }}
        scrollFunc={scrollToRef}
      />
      <Layout>
        <Modal
          visible={showModal}
          hideModal={() => {
            // videoRef.current.pause()
            setShowModal(false)
          }}
        >
          {/* <video
            ref={videoRef}
            className={indexStyles.video}
            width="320"
            height="240"
            controls
            autoplay
          >
            <source src={video} type="video/mp4" />
            Your browser does not support the video tag.
          </video> */}
          <div
            style={{
              height: 600,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            {/* <img
              style={{
                width: 600,
                heigth: 600,
                marginBottom: "0xp !important",
              }}
              src="https://cincoestrelascasadepaes.com.br/mimo_aniversario.jpeg"
            /> */}
            <iframe
              className={indexStyles.video}
              ref={videoRef}
              style={{ height: "100%", width: "100%" }}
              src="https://cincoestrelascasadepaes.com.br/catalogo_arraia.pdf"
              // src="http://localhost:8000/menu_dia_maes.pdf"
              frameborder="0"
              Rel="0"
              allow="accelerometer; autoplay; encrypted-media;
              gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
          </div>
        </Modal>

        <div className={indexStyles.container}>
          <Mainlogo ref={inicioRef} />
          <Promocoes ref={promocoesRef} />
          <Introducao ref={introducaoRef} />
          <ParalaxDivision img={isMobile ? paralax1_small : paralax1} />
          <PostList ref={blogRef} />
          <ParalaxDivision img={paralax2} />
          {/* <Parceiros ref={parceirosRef} /> */}
          <Map ref={mapRef} />
        </div>
      </Layout>
      <Footer />
      <Encomendas active={showEncomendas} setActive={setShowEncomendas} />
      <FaleConosco active={showFaleConosco} setActive={setShowFaleConosco} />
    </PageContext.Provider>
  )
}

export { PageContext }
export default IndexPage
