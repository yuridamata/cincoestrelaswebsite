---
title: "Como comer nosso pão rústico?"
call: "Confira uma dica de como comer o pão rústico da Cinco Estrelas."
date: "2021-03-13"
img: "pao_rustico.jpeg"
---

Corte o pão em rodelas, em um recipiente separe o azeite com manteiga e misture, depois pincele nos dois lados do pão.

Aqueça em umas das opções até ficar crocante dos dois lados (Forno, Frigideira ou Air Free)

Depois de pronto coloque queijo de sua preferência e como toque final, acrescente tomates picados com manjericão uma pitada de sal e pimenta a gosto.