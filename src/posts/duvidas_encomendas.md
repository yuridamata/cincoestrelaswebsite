---
title: "Dúvidas sobre as encomendas?"
call: "Pensando em você, selecionamos as dúvidas mais freqüentes, para que possa esclarecê-las imediatamente."
date: "2019-04-04"
img: "duvidas_encomendas.jpg"
---

Pensando em você, selecionamos as dúvidas mais freqüentes, para que possa esclarecê-las imediatamente. Caso sua dúvida não esteja relacionada abaixo, será um prazer te atendê-lo através de nossos telefones - 61 98418-4681 / 61 3381-4965.

##### Como faço encomendas?

Você pode entrar em contato com a nossa central de encomendas que estaremos prontos para atendê-lo. É importante que a encomenda seja feita com pelo menos 24h de antecedência por telefone e 48 horas por e-mail (encomendas-pedidos@cincoestrelascasadepaes.com.br), para que possamos atender suas expectativas. Para que sua encomenda seja efetivada você deverá efetuar o pagamento de 50% do valor da encomenda, com no mínimo 24 horas de antecedência, via transferência ou através do cartão de credito (enviamos um link para pagamento).

##### Vocês fazem encomendas via web?

Sim. Acesse o nosso e-commerce e faça o pedido no conforto da sua casa. [Acesse o e-commerce](https://cincoestrelascasadepaes.com.br/ecommerce/)

##### Qual o horário de funcionamento?

Nossas lojas estão abertas de 6h às 22h.

Encomendas via telefone das 7h às 20h.

##### Vocês fazem entregas?

Sim. Em toda região do DF. Confira nossa disponibilidade de horários com nossa equipe.

##### Como faço para calcular a quantidade de bolo, salgadinhos e docinhos para minha festa?

Para facilitar sua vida a Cinco Estrelas preparou para você o link Kits para Festas. Lá você encontra tudo para sua festa ficar saborosa com pouco trabalho. E só convidar os amigos e aproveitar.

##### Vocês tem produtos de linha light, integral e sem glúten?

Sim. Verifique a disponibilidade e variedade com nossa equipe.

##### Como faço pagamento em caso de entrega?

Para o serviço de entrega o pagamento é feito antecipadamente por transferência bancária ou pagamento via cartão de crédito

##### Quais as formas de pagamento?

Forma de pagamento: Dinheiro, cartão de crédito, débito, cartões de alimentação e/ou refeição. Não aceitamos cheque.
