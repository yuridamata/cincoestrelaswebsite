---
title: "Como calcular quantidade de comida por pessoa?"
call: "Para facilitar sua vida, padaria a Cinco Estrelas preparou para você uma lista de sugestão de quantidades de comida por pessoa. Assim você poderá estimar melhor o quanto pedir de cada item."
date: "2019-04-04"
img: "qtd_por_pessoa.jpg"
---


Para facilitar sua vida a Cinco Estrelas preparou para você uma lista de sugestão de quantidades de itens por pessoa.
Confira!


- Tábua de frios: 
    - pequena: p/ 10 à 15 pessoas
    - média: p/ 15 à 20 pessoas
    - grande: p/ 25 à 30 pessoas
- Cesta de pães: 
    - pequena: p/ 30 pessoas
    - média: p/ 15 à 20 pessoas
    - grande: p/ 25 à 30 pessoas
- Pão de metro:  Rende aproximadamente 20 fatias. Sugestão: 1 para cada 10 pessoas.
- Tortas salgadas: 150g à 200g por pessoa.
- Tortas salgadas de metro: 1 para cada 15 pessoas.
- Salgadinhos (assados, fritos ou folheados): 10 por pessoa.
- Mini sanduíches: 3 à 4 por pessoa
- Tortas doces: 100 à 150g por pessoa.
- Docinhos: 5 por pessoa.
- Bombons: 3 por pessoa
- Mini pão de hot dog ou mini pão de hambúrguer: 2 por pessoa.
- Pão de hot dog ou pão de hamburguer: 1 por pessoa
- Bebida não alcoólica: 400 ml por pessoa.