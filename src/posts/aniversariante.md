---
title: "Promoção Aniversariante"
call: "É seu aniversário? Nós Fazemos a sua festa e você ainda ganha um presente por nossa conta!!"
date: "2021-09-19"
img: "aniversarioNovo.jpg"
---

##### É SEU ANIVERSÁRIO ? NÓS FAZEMOS A SUA FESTA E VOCÊ AINDA GANHA UM PRESENTE POR NOSSA CONTA!!

&nbsp;
&nbsp;
&nbsp;

- ENCOMENDAS ACIMA R\$ 250,00 PARA O DIA DO SEU ANIVERSÁRIO.

- APRESENTE SUA IDENTIDADE

- ESCOLHA SEU PRESENTE:

  - 20 BRIGADEIROS
  - 1 MICRO TORTA TRADICIONAL 
  - 1 PÃO COM PASTA.
