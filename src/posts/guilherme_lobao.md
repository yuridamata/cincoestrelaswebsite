---
title: "Cinco Estrelas por Guilherme Lobão."
call: "Já se passaram vinte anos desde que o casal José Maria e Margareth Natalício deixou a cidade de Tiros (MG) e veio para o Distrito Federal ganhar a vida com uma tímida padaria."
date: "2019-04-04"
img: "cinco_estrelas.jpg"
---


Já se passaram vinte anos desde que o casal José Maria e Margareth Natalício deixou a cidade de Tiros (MG) e veio para o Distrito Federal ganhar a vida com uma tímida padaria. Apoiada pelos quatro filhos, atualmente proprietários do negócio, a Cinco Estrelas se tornou uma das principais padarias do Guará e agora conta com uma filial no Guará II.  
&nbsp;   
Na nova casa, o padrão permanece o mesmo da matriz: matéria-prima de boa qualidade somada a produtos frescos. Os pães, naturalmente, aparecem como as estrelas. A qualquer horário, o cliente encontra o brigite, macio e dotado de uma crosta de queijo — trata-se da receita de maior sucesso da marca.

&nbsp;   
Recheado, o novidade equilibra crocância e uma suculenta mistura de ricota, tomate e ervas. Como uma boa panificadora, o lugar oferece mesinhas para acomodar os interessados em lanchar por ali mesmo. Para acompanhar o cappuccino, sai da chapa um misto quente no pão francês.

&nbsp;   
Além do trigo: no local, há ainda um pequeno empório e uma confeitaria, que produz doces e tortas diariamente.

&nbsp;   
Área Especial 4, lote G/H. Edifício Olimpique, Guará II, fone: 3381-4961. 5h30/0h.
QI 4, bloco B, lojas 1/3, Guará I, fone: 3381-4965. 5h30/0h.

por Guilherme Lobão / fonte Veja Brasília.
